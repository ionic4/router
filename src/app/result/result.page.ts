import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {
  age: number;
  lowerLim: number;
  upperLim: number;

  constructor(public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.age = Number(this.activatedRoute.snapshot.paramMap.get('age'));
    this.upperLim = (220 - this.age) * 0.85;
    this.lowerLim = (220 - this.age) * 0.65;
  }

}
